package com.example.android.servicestart_thread;

import android.app.Service;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Random;

/**
 * Created by parth on 5/1/18.
 */

public class MyService extends Service {

    private int mRandomNumber;
    private boolean isRandomGenratorOn;

    private final int MIN=0;
    private final int MAX=100;

    class MyServiceBinder extends Binder{
        public MyService getService(){
            return MyService.this;
        }
    }
    private  IBinder mBinder=new MyServiceBinder();

    @Override
    public IBinder onBind(Intent intent) {
        Log.i("Service Demo","On Bind");
        return mBinder;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {

            Log.i("mySevice Class:","In onStart Command thread id "+Thread.currentThread().getId());
            isRandomGenratorOn=true;
        new Thread(new Runnable() {
            @Override
            public void run() {
                startRandomNumberGenerator();
            }
        }).start();

        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        stopRandomNumberGenerator();
        Log.i("MyService Class:","Service has been stopped");
    }

    private void startRandomNumberGenerator(){
        while(isRandomGenratorOn)
        {
            try{
                Thread.sleep(1000);
                if(isRandomGenratorOn){
                    mRandomNumber=new Random().nextInt(MAX)+MIN;
                    Log.i("MyService demo","Thread id "+Thread.currentThread().getId()+" Random number is "+mRandomNumber);
                }
            } catch (InterruptedException e) {
                Log.i("Myservice Demo","Thread Interrupted");
            }
        }
    }

    private void stopRandomNumberGenerator(){
        isRandomGenratorOn=false;
    }
    public int getRandomNumber(){
        return mRandomNumber;
    }
}
