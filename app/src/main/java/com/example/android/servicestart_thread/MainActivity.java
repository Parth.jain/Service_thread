package com.example.android.servicestart_thread;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    private Button mStartBtn,mStopBtn,mBindBtn,mUnbindBtn,mSetRandomBtn;
    private TextView textViewThreadCount;
    int count=0;

    private Intent serviceIntent;

    private boolean mStopLoop;
    private MyService myService;
    private boolean isServiceBound;
    private ServiceConnection serviceConnection;


    @Override
    protected void  onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Log.i("MainActivity Start Cls:","Thread for MainActivity Oncreate Method "+Thread.currentThread().getId());

        mStartBtn=findViewById(R.id.startBtn);
        mStopBtn=findViewById(R.id.stopBtn);
        mBindBtn=findViewById(R.id.bindBtn);
        mUnbindBtn=findViewById(R.id.unBindBtn);
        mSetRandomBtn=findViewById(R.id.getRandomBtn);
        textViewThreadCount=findViewById(R.id.textThreadCount);
        serviceIntent=new Intent(getApplicationContext(),MyService.class);
        mStartBtn.setOnClickListener(this);
        mStopBtn.setOnClickListener(this);
        mBindBtn.setOnClickListener(this);
        mUnbindBtn.setOnClickListener(this);
        mSetRandomBtn.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.startBtn:
            //mStopLoop=true;
            startService(serviceIntent);
            break;

            case R.id.stopBtn:
                stopService(serviceIntent);
                break;
            case R.id.bindBtn:
                bindService();
                break;

            case R.id.unBindBtn:
                unbindService();
                break;

            case R.id.getRandomBtn:
                setRandomNumber();
                break;
        }
    }


    private void bindService() {
    if(serviceConnection==null){
        serviceConnection=new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
            MyService.MyServiceBinder myServiceBinder=(MyService.MyServiceBinder)service;
            myService=myServiceBinder.getService();
            isServiceBound=true;
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
            isServiceBound=false;

            }
        };
    }
    bindService(serviceIntent,serviceConnection, Context.BIND_AUTO_CREATE);
    }

    private void unbindService() {
    if(isServiceBound)
    {
        unbindService(serviceConnection);
        isServiceBound=false;
    }
    }

    private void setRandomNumber() {
        if(isServiceBound) {
            textViewThreadCount.setText("Random number: " + myService.getRandomNumber());
        }
        else{

            textViewThreadCount.setText("Service Not Bound");
        }
    }

}
